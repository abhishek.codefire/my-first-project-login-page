<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "abhi".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $photo
 */
class Abhi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'abhi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'password', 'photo'], 'required'],
            [['first_name', 'last_name', 'email', 'photo'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 50],
            [['email'], 'unique'],
            [['photo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'photo' => 'Photo',
        ];
    }
}
