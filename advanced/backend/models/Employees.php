<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $pasword1
 * @property string $confirm_password
 * @property string|null $profile_pic
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'pasword1', 'confirm_password'], 'required'],
            [['first_name', 'last_name', 'email', 'profile_pic'], 'string', 'max' => 255],
            [['pasword1', 'confirm_password'], 'string', 'max' => 30],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'pasword1' => 'Pasword1',
            'confirm_password' => 'Confirm Password',
            'profile_pic' => 'Profile Pic',
        ];
    }
}
