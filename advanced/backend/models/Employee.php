<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $pwd
 * @property string $photo
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'pwd'], 'required'],
            ['photo', 'required'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 255],
            ['photo', 'string', 'max' => 255],
            [['pwd'], 'string', 'max' => 50],
            [['email'], 'unique'],
        ];
    }
  
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'pwd' => 'Password',
            'photo' => 'Photo',
        ];
    }
}
