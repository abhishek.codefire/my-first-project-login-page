<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id' => 'view']); ?>
<h1>View User</h1>
<img src='assets/images/<?= $model->photo; ?>' height="200" width="200">
<table>
    <tr>
        <td><?= $model->first_name; ?> <?= $model->last_name; ?></td>

    </tr>
    <tr>
        <td><?= $model->email; ?></td>
    </tr>
</table>
<?php Pjax::end(); ?>