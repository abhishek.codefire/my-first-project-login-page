<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<!-- ['id' => $model->formName()] -->
<!-- [
    'options' => [
        'id' => 'create-product-form'
    ]
] -->
<h1>Add User</h1>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' => $model->formName()]); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'pwd')->passwordInput(['maxlength' => true])->label('Password') ?>

<?= $form->field($model, 'photo')->fileInput()->label('Profile Photo'); ?>

<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>