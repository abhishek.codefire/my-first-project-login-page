<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

?>
<h1>Yii2  CRUD Application AJAX Design By Abhishek</h1><br><br> 
<?= Html::button('Create User', ['value' => Url::to('index.php?r=employee/create'), 'class' => 'addButton btn btn-primary', 'onclick' => 'viewClick(this.getAttribute("class"))']); ?>

<?php
Modal::begin([
    //'header' => '<h4>Add User</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
<?php Pjax::begin(['id' => 'index']); ?>
<br>

<style> 
 h1
      {
        text-align:center;
        background-color:orange;
        color:black;
      }  
table th,td{   
    
    text-align:center;  
}  
th
{
    background-color:green;
    color:white;
} 
</style>  

<table border="1" class='table table-striped table-hover table-bordered'>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        </th>
        <th>Action</th>
    </tr>
    <?php foreach ($model as $field) :
        $encodedData = json_encode($field);
    ?>
        <tr>
        <td><?= $field['id']; ?></td>
            <td><?= $field['first_name']; ?> <?= $field['last_name']; ?></td>
            <td><?= $field['email']; ?></td>
            <td>
                <?= Html::button('View', ['value' => '', 'class' => 'viewButton btn btn-success', 'onclick' => 'viewClick(this.getAttribute("class"),this)', 'data-model' => $encodedData]); ?>
                <?= Html::button('Edit', ['value' => '', 'class' => 'editButton btn btn-info', 'onclick' => 'viewClick(this.getAttribute("class"),this)', 'data-model' => $encodedData]); ?>
                <?= Html::button('Delete', ['value' => '', 'class' => 'deleteButton btn btn-danger', 'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],'onclick' => 'viewClick(this.getAttribute("class"),this)', 'data-model' => $encodedData]); ?>

        </tr>
    <?php endforeach; ?>
</table>
<?= LinkPager::widget(['pagination' => $pagination]) ?>
<?php Pjax::end(); ?>