<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
?>
<div class="employees-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'email:email',
            'pasword1',
            'confirm_password',
            'profile_pic',
        ],
    ]) ?>

</div>
