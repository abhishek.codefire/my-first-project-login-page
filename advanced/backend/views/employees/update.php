<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
?>
<div class="employees-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
