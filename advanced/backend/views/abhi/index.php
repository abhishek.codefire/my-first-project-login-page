<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Abhi;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AbhiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Abhi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abhi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Abhi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php
$dataProvider = new ActiveDataProvider([
'query' => Abhi::find(),
 //above is the active query
'pagination' => [
'pageSize' => 2,
],
]);
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'email:email',
            //'password',
            //'photo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
