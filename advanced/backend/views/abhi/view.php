<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Abhi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Abhi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="abhi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <img src='assets/images/<?= $model->photo; ?>' height="200" width="200">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'first_name',
            'last_name',
            'email:email',
            //'password',
            //'photo',
        ],
    ]) ?>

</div>
