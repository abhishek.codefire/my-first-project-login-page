<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Abhi */

$this->title = 'Create Abhi';
$this->params['breadcrumbs'][] = ['label' => 'Abhi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abhi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
