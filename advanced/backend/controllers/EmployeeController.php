<?php

namespace backend\controllers;

use Yii;
use backend\models\Employee;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\data\Pagination;
use \yii\web\Response;
use yii\helpers\Html;

/**  
 * manual CRUD  
 **/
class EmployeeController extends Controller
{
    /**  
     * Create  
     */
    public function actionCreate()
    {

        $model = new Employee();

        // new record   
        if ($model->load(Yii::$app->request->post()))
        {
            $model->photo = UploadedFile::getInstance($model, 'photo');
            $fileName = time() . '.' . $model->photo->extension;
            $model->photo->saveAs('assets/images/' . $fileName);
            $model->photo = $fileName;
            //$data=CHtml::listData($model,'cityid','name');
            $model->save();
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model
            ]);
        }
    }
    public function actionIndex()
    {
        $query = Employee::find();
        $pagination = new Pagination([
            'defaultPageSize' => 3,
            'totalCount' => $query->count(),
        ]);
        $emp = $query->orderBy('first_name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        return $this->render('index', ['model' => $emp, 'pagination' => $pagination,]);
    }
    public function actionUpdate($id)
    {

        $model = Employee::find()->where(['id' => $id])->one();

        // $id not found in database   
        if ($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // update record   
        if ($model->load(Yii::$app->request->post())) {
            $model->photo = UploadedFile::getInstance($model, 'photo');
            $fileName = time() . '.' . $model->photo->extension;
            $model->photo->saveAs('assets/images/' . $fileName);
            $model->photo = $fileName;
            //$data=CHtml::listData($model,'cityid','name');
            $model->save();
            return $this->redirect(['index', 'id' => $model->id]);
            echo 1;
        } else {
            return json_encode($this->renderAjax('update', ['model' => $model]));
        }
    }
    public function actionView($id)
    {
        $output = "";
        //$id = $_POST('id');
        $model = Employee::find()->where(['id' => $id])->one();

        // $id not found in database   
        if ($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // $model[] = $this->renderPartial("view", array('model' => $model), true);

        // echo CJSON::encode($model);

        //$output = $model->first_name;
        //echo json_encode($output);
        return json_encode($this->renderAjax('view', ['model' => $model]));
    }
    public function actionDelete($id)
    {
        $model = Employee::findOne($id);

        // $id not found in database   
        if ($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');

        // delete record   
        $model->delete();
        return json_encode("1");
        //return json_encode($this->redirect(['index']));
    }
    public function actionGrid()
    {
        return $this->render('resultgrid');
    }
}