function viewClick(className, e) {
    //for add button
    if (className == 'addButton btn btn-primary') {
        $('#modal').modal('show')
            .find('#modalContent')
            .load($('.addButton').attr('value'));
    }
    //for viewButton
    if (className == 'viewButton btn btn-success') {
        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/Codefire/yiiproject/advanced/backend/web/index.php?r=employee/view&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $('#modalContent').html(data);
                $('#modal').modal('show');
            },
            error: function (xhr, status, error, data) {
                alert('There was an error with your request.' + data);
            }
        })
    }
    //for editButton
    if (className == 'editButton btn btn-info') {
        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/Codefire/yiiproject/advanced/backend/web/index.php?r=employee/update&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $('#modalContent').html(data);
                $('#modal').modal('show');
            },
            error: function (xhr, status, error) {
                alert('There was an error with your request.' + xhr.responseText);
            }
        })
    }
    // for deleteButton
    if (className == 'deleteButton btn btn-danger') {
        var getData = $(e).data('model');
        $.ajax({
            url: "http://localhost/Codefire/yiiproject/advanced/backend/web/index.php?r=employee/delete&id=" + getData.id,
            type: "post",
            dataType: 'json',
            success: function (data) {
                $.pjax.reload({ container: '#index' });
            },
            error: function (xhr, status, error) {
                alert('There was an error with your request.' + xhr.responseText);
            }
        })
    }
}
