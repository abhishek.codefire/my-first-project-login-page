<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'Tempo/assets/css/style.css',
    ];
    public $js = [
	 "Tempo/assets/vendor/jquery/jquery.min.js",
  "Tempo/assets/vendor/bootstrap/js/bootstrap.bundle.min.js",
  "Tempo/assets/vendor/jquery.easing/jquery.easing.min.js",
  "Tempo/assets/vendor/php-email-form/validate.js",
  "Tempo/assets/vendor/isotope-layout/isotope.pkgd.min.js",
  "Tempo/assets/vendor/venobox/venobox.min.js",
  "Tempo/assets/vendor/owl.carousel/owl.carousel.min.js",

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
