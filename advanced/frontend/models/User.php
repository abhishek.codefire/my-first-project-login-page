<?php

namespace frontend\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public function rules()
    {
        return [
            [['profile_pic'], 'string', 'max' => 255],
        ];
    }
}