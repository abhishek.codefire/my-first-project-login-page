<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$cssContent = '
.users2-form{
    background-color:black;
    color:white;
    width:50%;
    margin:auto;
    border-radius:10px;
    text-align: center;
    margin-top:40px;
    padding-bottom:100px;
    border:solid white 2px;
    height:50%;
    
}
.form{
    width:50%;
    display:inline-block;
    text-align:left;
}

.pic{
    float:left;
    padding:15px;
    display:block;
}
.h2{
    margin-top:5px;
    margin-bottom:5px;

}
.nn{
    float:left;
    margin-left:40px;
    margin-top:10px;
}


';
$this->registerCss($cssContent);
$jsContent = 'put your javascript content here';
$this->registerJs($jsContent);
?>
<div class="users2-form">

    <?php
    if ($model->profile_pic === null) {
        echo "<img class='pic' src='assets/images/default.jpg' height='200' width='200'>";
    } else {
    ?>
        <img class='pic' src='assets/images/<?= $model->profile_pic; ?>' height='200' width='200'>
    <?php
    }

    ?>
    <h2 class="h2"><?= $model->username; ?></h2>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form']]); ?>

    <?= $form->field($model, 'profile_pic')->fileInput()->label('Change Profile Photo'); ?>

    <?= Html::submitButton($model->isNewRecord ? 'Update Profile' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>
    <?= Html::beginForm(['/site/passReset'], 'post') ?>
    
</div>