<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Password Reset';
// $this->params['breadcrumbs'][] = $this->title;
$cssContent = '
.site-request-password-reset{
    background-color:#222222;
    color:white;
    width:50%;
    margin:auto;
    border-radius:10px;
    padding:10px;
    text-align: center;
    margin-top:40px;
    border:solid white 2px;
}
form{
    margin-left: 80%;
    text-align: center;
    width:100%;

';
$this->registerCss($cssContent);
$jsContent = 'put your javascript content here';
$this->registerJs($jsContent);
?>
<div class="site-request-password-reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'new_password')->passwordInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'confirm_password')->passwordInput(['autofocus' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>