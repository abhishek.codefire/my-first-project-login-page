<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$cssContent = '
.site-login{
    background-color:#222222;
    color:white;
    width:50%;
    margin:auto;
    border-radius:10px;
    padding:10px;
    text-align: center;
    margin-top:40px;
    border:solid white 2px;
}
#login-form{
    margin-left: 80%;
    text-align: center;
    width:100%;

';
$this->registerCss($cssContent);
$jsContent = 'put your javascript content here';
$this->registerJs($jsContent);
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                <?= Html::a('Forgot Password?', ['site/forgotpass']) ?>.

                </div>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>  

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
