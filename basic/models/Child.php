<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "myuser".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $profile_pic
 */
class Child extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'child';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'password', 'profile_pic'], 'required'],
            [['first_name', 'last_name', 'email', 'profile_pic'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 50],
            [['email'], 'unique'],
        ];
    }
}