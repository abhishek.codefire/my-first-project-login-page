<?php
namespace app\controllers;   
   
use Yii;   
use app\models\Child; 
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;  
use yii\web\Controller; 
use yii\data\Pagination;
  
/**  
 * manual CRUD  
 **/   
class ChildController extends Controller   
{    
    /**  
     * Create  
     */   
    public function actionCreate()   
    {   
        $model = new Child();   
   
        // new record 
        if($model->load(Yii::$app->request->post())){ 
            $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            $fileName = time() . '.' . $model->profile_pic->extension;
            $model->profile_pic->saveAs('assets/images/' . $fileName);
            $model->profile_pic = $fileName; 
            $model->save(); 
            return $this->redirect(['index']);   
        }   
                   
        return $this->render('create', ['model' => $model]);   
    }   
  
    /**  
     * Read  
     */   
    public function actionIndex()   
    {  
        $childd = Child::find(); 
       
        $pagination = new Pagination(['totalCount' => $childd->count(), 'pageSize'=>2]);
        $child = $childd->offset($pagination->offset)
        ->limit($pagination->limit)
        ->all();
        return $this->render('index', [
            'model' => $child,
            'pagination' => $pagination,
        ]);   
    } 
    
    /**  
     * Edit  
     * @param integer $id  
     */   
    public function actionEdit($id)   
    {   
        $model = Child::find()->where(['id' => $id])->one();   
   
        // $id not found in database   
        if($model === null)   
            throw new NotFoundHttpException('The requested page does not exist.');   
           
        // update record   
        if($model->load(Yii::$app->request->post()))
     
        {  
            $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            $fileName = time() . '.' . $model->profile_pic->extension;
            $model->profile_pic->saveAs('assets/images/' . $fileName);
            $model->profile_pic = $fileName; 
            $model->save(); 
            return $this->redirect(['index','id' => $model->id]);   
        }   
           
        return $this->render('edit', ['model' => $model]);   
    }

    /**  
    * Delete  
     * @param integer $id  
     */   
    public function actionDelete($id)   
    {   
        $model = Child::findOne($id);   
          
       // $id not found in database   
       if($model === null)   
           throw new NotFoundHttpException('The requested page does not exist.');   
              
       // delete record   
       $model->delete();   
          
       return $this->redirect(['index']);   
    }  
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            
        ]);
    }
    protected function findModel($id)
    {
        if (($model = Child::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}  
 