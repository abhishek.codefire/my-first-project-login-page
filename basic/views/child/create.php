<?php

use yii\helpers\Html;

$this->title = 'Create Child';
$this->params['breadcrumbs'][] = ['label' => 'Child', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-create">
<?= $this->render('_form', [
    'model' => $model,
]) ?>
</div>
