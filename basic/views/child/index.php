<?php   
use yii\helpers\Html;
use app\models\Child; 
use yii\widgets\LinkPager;


$this->title = 'Child';
$this->params['breadcrumbs'][] = $this->title;
?>   
   
<style> 
 h1
      {
        text-align:center;
        background-color:orange;
        color:black;
      }  
table th,td{   
    padding: 20px; 
    margin:30px;
    text-align:center;  
}  
th
{
    background-color:green;
    color:white;
} 
</style>   
<h1>Yii2 CRUD Application Design By Abhishek</h1><br><br><br> 
<?= Html::a('Create', ['child/create'], ['class' => 'btn btn-primary']); ?><br><br>   
<table class="table table-bordered table table-hover  table-striped" border="2"> 
   <tr> 
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th> 
        <th>Email</th>
        <th>View</th>
        <th>Edit</th>
        <th>Delete</th>
        
    </tr> 
    <?php foreach($model as $field){ ?>
    
    <tr>  
        <td><?=$field->id; ?></td>
        <td><?= $field->first_name; ?></td>
        <td><?= $field->last_name; ?></td>
        <td><?= $field->email; ?></td>
        <td><?= Html::a("View", ['child/view', 'id' => $field->id],['class' => 'btn btn-info']);?></td>
        <td><?= Html::a("Edit", ['child/edit', 'id' => $field->id],['class' => 'btn btn-success']); ?></td>
        <td><?= Html::a("Delete", ['child/delete', 'id' => $field->id],['class' => 'btn btn-danger',
        'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
       ]); ?></td>
    </tr>
    <?php }
      echo LinkPager::widget([
        'pagination' => $pagination,
    ]);?>
       </table>
  
