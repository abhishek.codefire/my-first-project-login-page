<?php

use yii\helpers\Html;

$this->title = 'Update Child: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'child', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="child-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
