<?php   
use yii\helpers\Html;   
use yii\widgets\ActiveForm;   
?>   
<div class="child-form">  

<?php $form = ActiveForm::begin(); ?>   
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

<?= $form->field($model, 'profile_pic')->fileInput(['maxlength' => true]) ?>

    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->   
     isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
   
   <?php ActiveForm::end(); ?>
   </div>