<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\color\ColorInput;
use yii\jui\DatePicker;

$items = array("Male", "Female", "Other");
$qualification = [1 => "10th", 2 => "12th", 3 => "graduate", 4 => "post graduate", 5 => "phd"];
?>
<style>
    div.required label.control-label:after {
        content: " *";
        color: red;
    }
</style>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'fname')->label('First Name') ?>
<?= $form->field($model, 'lname')->label('Second Name') ?>
<?= $form->field($model, 'phone')->label('Phone Number') ?>
<?= $form->field($model, 'email')->input('email')->label('Your Email') ?>
<?= $form->field($model, 'color')->input('color')->widget(ColorInput::classname(), ['options' => ['placefolder' => 'Select color...']])->label('Enter your favorite color') ?>
<?= $form->field($model, 'dob')->input('date')->widget(DatePicker::classname(), ['dateFormat' => 'php:Y-m-d'])->label('Date of Birth') ?>
<?= $form->field($model, 'gender')->radioList(
    [
        1 => 'Male',
        2 => 'Female',
        3 => 'Other'
    ],
    ['prompt' => 'Select Gender']
)->label('Gender') ?>
<?= $form->field($model, 'qual')->dropDownList($qualification, ['prompt' => 'Select option'])->label('Qualification') ?>
<?= $form->field($model, 'pass')->passwordInput()->label('Password') ?>
<?= $form->field($model, 'cpass')->passwordInput()->label('Confirm Password') ?>
<?= $form->field($model, 'resume')->input('file')->label('Upload Resume') ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>